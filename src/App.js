import React, { Component } from 'react'
import PropTypes from 'prop-types'
// eslint-disable-next-line
import { Switch, Route, withRouter, Link } from 'react-router-dom'
// eslint-disable-next-line
import { inject, observer } from 'mobx-react'
import { Redirect } from 'react-router'
import RctTextSelectorComponent from '@4geit/rct-text-selector-component'

const title = 'Google Pixel phones to be unveiled October 4'
const sample = `The firm may also launch its voice-activated AI home audio speaker, a VR headset and a 4K Chromecast

Google will unveil its latest Pixel smartphones on October 4, the firm has suggested through a teaser website.

It is believed the Nexus brand name will be dropped in favour of two new models: the Pixel and the Pixel XL.

Current rumours around the devices – which have been codenamed Sailfish and Marlin – say the devices will be 5-inch and 5.5-inch phones. At Google's developer conference in May CEO Sundar Pichai hinted that the new line would includes additional Google features built on top of the Android operating system. As such, Pixel is expected to become a premium brand.

The teaser website shows a search bar that morphs into the outline of a phone, before flashing with various images. The event will take place in San Francisco at 9AM PST (5PM BST), and be streamed on YouTube.
Leaked images of the Pixel phones
Leaked images of the Pixel phones
Android Police

At present the company controls the design and marketing of its Nexus products, with the manufacturing of the devices outsourced to third parties. This, according to Pichai earlier this year, will not change.

But there could also be more to come from the event. A previous report from Android Police has said the event will be packed with Google's latest products.

Rumoured to be launching that day will be a Google virtual reality headset, under the guise of the Google Daydream project; a 4K Chromecast, and its highly anticipated AI personal assistant device.

At its developer conference, Google announced a new voice-controlled, artificial intelligence Home speaker that will connect to devices around a building and be able to control them.
The artificial intelligence based Google Home speaker
The artificial intelligence based Google Home speaker
Google

The device will use a Google Assistant, which is being built into a new Google messages app called Allo – which is rumoured to be launching before the October event. "With a simple voice command, you can ask Google Home to play a song, set a timer for the oven, check your flight, or turn on your lights," the firm said at the time.

Any home device would be a rival to Amazon's Echo, which launched in the UK on September 16. The Echo uses Amazon's own personal assistant, Alexa, to control internet of things devices as well as provide news, weather, and travel updates.

Although the teaser website doesn't reference any of these products, Google previously said its Home device would launch before the end of 2016.`
function PrivateRoute({ component: RouteComponent, authed, ...rest }) {
  if (authed) {
    return <Route {...rest} render={props => <RouteComponent {...props} />} />
  }
  return (
    <Route
      {...rest}
      render={props => (
        <Redirect
          to={{
            pathname: '/login',
            state: {
              // eslint-disable-next-line react/prop-types
              from: props.location,
            },
          }}
        />
      )}
    />
  )
}
PrivateRoute.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  component: PropTypes.any.isRequired,
  authed: PropTypes.bool.isRequired,
}

// @inject('swaggerClientStore', 'commonStore')
@withRouter
@observer
class App extends Component {
  static propTypes = {
    // commonStore: PropTypes.instanceOf(RctCommonStore).isRequired,
    // swaggerClientStore: PropTypes.instanceOf(RctSwaggerClientStore).isRequired,
    // eslint-disable-next-line react/forbid-prop-types, react/no-unused-prop-types
    location: PropTypes.object.isRequired,
    // eslint-disable-next-line react/forbid-prop-types, react/no-unused-prop-types
    classes: PropTypes.object.isRequired,
  }

  // async componentWillMount() {
  //   await this.props.swaggerClientStore.buildClient()
  //   this.props.commonStore.setAppLoaded()
  // }

  render() {
    // const { isLoggedIn, appLoaded } = this.props.commonStore
    // if (!appLoaded) {
    //   return (<div>Loading...</div>)
    // }
    return (
      <div>
        <RctTextSelectorComponent
          title={title}
          sample={sample}
        />
      </div>
    )
  }
}

export default App
